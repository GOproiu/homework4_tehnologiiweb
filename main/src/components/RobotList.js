import React, { Component } from "react";
import RobotForm from "./RobotForm";
import Robot from "./Robot";
import RobotStore from "../stores/RobotStore";

class RobotList extends Component {
  constructor() {
    super();
    this.state = {
      robots: [],
    };
  }

  addRobot(r) {
    this.store.addRobot(r);
  }

  componentDidMount() {
    this.store = new RobotStore();
    this.setState({
      robots: this.store.getRobots(),
    });
    this.store.emitter.addListener('UPDATE', () => {
      this.setState({
        robots: this.store.getRobots(),
      });
    });
    this.addRobot = this.addRobot.bind(this);
  }
  
  
  render() {
    return (
      <div>
        {this.state.robots.map((e, i) => (
          <Robot item={e} key={i} />
        ))}
        <RobotForm onAdd={this.addRobot} />
      </div>
    );
    
  }
}

export default RobotList;