// Aplicația se desenează fără eroare; (0.5 pts)
// Se desenează o componentă RobotForm; (0.5 pts)
// RobotForm are o proprietate onAdd care conține o funcție; (0.5 pts)
// Dat fiind că input-urile pentru proprietățile robotului au id-urile name, type and mass 
// și că butonul de adăugare are valoarea add se poate adăuga un robot;(0.5 pts)
// Robotul adăugat are valorile corecte în proprietăți. (0.5 pts)
import React, {Component} from "react";

const divStyle = {
    fontSize: 20,
    marginLeft: '160px',
}

const inputStyle = {
  color: '#86C232',
  background: '#222629',
  border: '2px solid #86C232',
};

const buttonStyle = {
  color: '#222629',
  background: '#86C232',
  border: '2px solid #222629',
  marginLeft: '50px',
  fontSize: 20,
};

const labelStyle = {
    color: '#86C232',
}

class RobotForm extends Component{
    constructor(){
        super();
        this.state = {
            name : "",
            type : "",
            mass : ""
        };
        this.submit = this.submit.bind(this);
        
    };
    
    submit(event) {
        event.preventDefault();
        this.setState({
            name: "",
            type: "",
            mass: ""
        });
    }
    
    render(){
        return(
            <div style={divStyle} >
                <form onSubmit={this.submit}>
                    <br></br>
                    <label style={labelStyle}>Insert name</label>
                    <br></br>
                    <input style={inputStyle} id="name" value={this.state.name} onChange={(event) => this.setState({ name: event.target.value })}/>
                    <br></br>
                    <label style={labelStyle}>Insert type</label>
                    <br></br>
                    <input id="type" style={inputStyle} value={this.state.type} onChange={(event) => this.setState({ type: event.target.value })}/>
                    <br></br>
                    <label style={labelStyle}>Insert mass</label>
                    <br></br>
                    <input id="mass" style={inputStyle} value={this.state.mass} onChange={(event) => this.setState({ mass: event.target.value })}/>
                    <br></br>
                    <button style={buttonStyle} type="submit" value="add" onClick={() => this.props.onAdd(this.state)}>Submit</button>
                </form>
            </div>
        )
    };
}

export default RobotForm;