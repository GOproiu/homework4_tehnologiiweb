import React, { Component } from 'react'

const divStyle = {
    fontSize: 20,
    color: '#A4A71E',
    marginLeft: '45px',
}

class Robot extends Component {
  render() {
  	let {item} = this.props
    return (
      <div style={divStyle}>
  		Hello, my name is {item.name}. I am a {item.type} and weigh {item.mass}
      </div>
    )
  }
}

export default Robot
