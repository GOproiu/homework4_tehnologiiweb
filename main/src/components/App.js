import React, { Component } from 'react'
import RobotList from './RobotList'

const divStyle = {
    fontSize: 20,
    background: '#222629',
    color: '#A4A71E',
    marginLeft: '400px',
    width: '40%',
}

const pStyle = {
    fontSize: 20,
    color: '#A4A71E',
    marginLeft: '200px',
}

const headerStyle = {
    fontSize: 20,
    color: '#A4A71E',
    marginLeft: '160px',
}


class App extends Component {
  render() {
    document.body.style = 'background: #222629';
    return (
      <div style={divStyle} >
        <br></br>
        <header style={headerStyle}>Homework 4 WebTech</header>
      	<p style={pStyle}>Robots list</p>
      	<RobotList />
      	<br></br>
      </div>
    )
  }
}

export default App
